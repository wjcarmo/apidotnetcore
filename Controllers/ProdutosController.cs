using System.Collections.Generic;
using Commander.Data;
using Commander.Models;
using Microsoft.AspNetCore.Mvc;

namespace Commander.Controllers
{

[Route("api/produtos")]
[ApiController]
public class ProdutosController : ControllerBase
 {
    private readonly IProdutos _repository;

public ProdutosController(IProdutos repository){

    _repository = repository;
}
        //GET api/produtos
        [HttpGet()]
        public ActionResult<List<Produtos>> GetListaDeProdutos()
        {
            var lista = _repository.GetListaDeProdutos();

            return Ok(lista);
        }
      
      
        //GET api/produtos/{id}
        [HttpGet("{id}")]
        public ActionResult <Command> GetAppProdutosById(int id)
        {
            var commandItem = _repository.GetAppProdutosById(id);

            return Ok(commandItem);
        }    
 }
   
}