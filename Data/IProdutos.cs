using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data
{
    public interface IProdutos
    {
        IEnumerable<Produtos> GetAppProdutos();
        Produtos GetAppProdutosById(int id);
        List<Produtos> GetListaDeProdutos();

    }
}