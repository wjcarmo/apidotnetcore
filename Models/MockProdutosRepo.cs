using System.Collections.Generic;
using Commander.Data;
using Commander.Models;

namespace Commander.Models
{
    public class MockProdutosRepo : IProdutos
    {
        public IEnumerable<Produtos> GetAppProdutos()
        {
            var produtos = new List<Produtos>
            {
                new Produtos{Id=0, Descricao="Arroz Agulhinha Tipo I", Unidade ="KG", ValorUnitario=10.00M,Validade="31/12/2020"},
                new Produtos{Id=1, Descricao="Açúcar Cristal claro", Unidade ="KG", ValorUnitario=5.00M,Validade="31/12/2020"},
                new Produtos{Id=2, Descricao="Feijão Carioca Novo Tipo I", Unidade ="KG", ValorUnitario=7.00M,Validade="31/12/2020"}
            };

            return produtos;
        }

       public Produtos GetAppProdutosById(int id)
        {
           return new Produtos{Id=id, Descricao="Arroz Agulhinha Tipo I", Unidade ="KG", ValorUnitario=10.00M,Validade="31/12/2020"};
        }

        public List<Produtos> GetListaDeProdutos()
        {
              var produtos = new List<Produtos>
            {
                new Produtos{Id=0, Descricao="Arroz Agulhinha Tipo I", Unidade ="KG", ValorUnitario=10.00M,Validade="31/12/2020"},
                new Produtos{Id=1, Descricao="Açúcar Cristal claro", Unidade ="KG", ValorUnitario=5.00M,Validade="31/12/2020"},
                new Produtos{Id=2, Descricao="Feijão Carioca Novo Tipo I", Unidade ="KG", ValorUnitario=7.00M,Validade="31/12/2020"},
                new Produtos{Id=2, Descricao="Feijão Preto Novo Tipo I", Unidade ="KG", ValorUnitario=7.00M,Validade="31/12/2020"},
                new Produtos{Id=2, Descricao="Macarrão Espaguete", Unidade ="KG", ValorUnitario=4.00M,Validade="31/12/2020"}
            };

            return produtos;
        }
    }
}