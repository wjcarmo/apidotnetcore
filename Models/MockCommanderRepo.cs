using System.Collections.Generic;
using Commander.Models;

namespace Commander.Data
{
    public class MockCommanderRepo : ICommanderRepo
    {
        public IEnumerable<Command> GetAppCommands()
        {
            var commands = new List<Command>
            {
                new Command{id=0, HowTo="How To 00", Line ="Boil water 00", Platform="Kettle & Part 00"},
                new Command{id=1, HowTo="How To 01", Line ="Boil water 01", Platform="Kettle & Part 01"},
                new Command{id=2, HowTo="How To 02", Line ="Boil water 02", Platform="Kettle & Part 03"}
            };

            return commands;
        }

        public Command GetCommandById(int id)
        {
            return new Command{id=0, HowTo="Boil an egg", Line ="Boil water", Platform="Kettle & Part"};
        }
    }
}