namespace Commander.Models
{
    public class Produtos
    {
        public bool Cesta { get; set; }
        public int Id { get; set; }
        public string Descricao { get; set; }
        public string Unidade { get; set; }
        public decimal ValorUnitario { get; set; }
        public string Validade { get; set; }
    }
}